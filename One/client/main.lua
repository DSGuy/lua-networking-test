-- Code written by Oladeji Sanyaolu (Network_Test - client) 06/7/2019

sock   = require 'sock'
bitser = require 'bitser'
player = require 'player'

server_x = 0
server_y = 0
server_w = 0
server_h = 0

net_update_rate = 1/60
net_update = 0

love.load = function()
    player.load(300, 300)
    client = sock.newClient("localhost", 22122)
    client:setSerialization(bitser.dumps, bitser.loads)

    -- Called when a connection is made to the server
    client:on("connect", function(data)
        print("Client connected to the server.")

    end)

    -- Called when the client disconnects from the server
    client:on("disconnect", function(data)
        print("Client disconnected from the server.")
    end)

    -- Custom callback, called whenever you send the event from then server
    client:on("position_server", function(data)
        server_x = data[1]
        server_y = data[2]
        server_w = data[3]
        server_h = data[4]
    end)

    -- Connect to the server
    client:connect()

end

love.update = function(dt)
    client:update()

    player.update(dt)
    update_server(dt)
end

love.draw = function()
    player.draw()
    love.graphics.rectangle('fill', server_x, server_y, server_w, server_h)
    player.debug(server_x, server_y)
end

update_server = function(dt)
    net_update = net_update + 1
    
    if net_update >= net_update_rate then
        client:send("position_client", {
            player.getX(),
            player.getY(),
            player.getW(),
            player.getH(),
        })
        print("Data sent")

        net_update = 0
    end
end

