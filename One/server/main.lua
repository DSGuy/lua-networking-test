-- Code written by Oladeji Sanyaolu (Network_Test - server) 06/7/2019

sock   = require 'sock'
player = require 'player'
bitser = require 'bitser'

client_x = 0
client_y = 0
client_w = 0
client_h = 0

net_update_rate = 1/60
net_update = 0

love.load = function()
    player.load(100, 100)
    server = sock.newServer("*", 22122, 2)
    server:setSerialization(bitser.dumps, bitser.loads)

    server:on("connect", function(data, client)
        print("Client has connected")
    end)

    server:on("disconnect", function(data, client)
        print("Client has disconnected")
    end)

    -- Custom callback, called whenever you send the event from then client
    server:on("position_client", function(data, client)
        client_x = data[1]
        client_y = data[2]
        client_w = data[3]
        client_h = data[4]
    end)

end

love.update = function(dt)
    server:update()
    player.update(dt)

    update_client(dt)
end

love.draw = function()
    player.draw()
    love.graphics.rectangle('fill', client_x, client_y, client_w, client_h)
    player.debug(client_x, client_y)
end

update_client = function(dt)
    net_update = net_update + 1
    
    if net_update >= net_update_rate then
        server:sendToAll("position_server", {
            player.getX(),
            player.getY(),
            player.getW(),
            player.getH(),
        })
        print("Data sent")

        net_update = 0
    end
end