-- Code written by Oladeji Sanyaolu (player) 06/7/2019

player = {}
player.index = {}
player.__index = player

player.load = function(x, y)
    new_player = {}
    new_player.x = x
    new_player.y = y
    new_player.w = 10
    new_player.h = 10
    new_player.sp = 3
    table.insert(player.index, new_player)
end

player.update = function(dt)
    player.movement(dt)
    player.boundary()
end

player.movement = function(dt)
    for _,p in ipairs(player.index) do 
        if love.keyboard.isDown('up') then
            p.y = p.y - p.sp
        end
        if love.keyboard.isDown('down') then
            p.y = p.y + p.sp
        end
        if love.keyboard.isDown('left') then
            p.x = p.x - p.sp
        end
        if love.keyboard.isDown('right') then
            p.x = p.x + p.sp
        end
    end
end

player.boundary = function()
    for _,p in ipairs(player.index) do
        if p.x > love.graphics.getWidth() - p.w then
            p.x = love.graphics.getWidth() - p.w
        elseif p.x < 0 then 
            p.x = 0
        end

        if p.y > love.graphics.getHeight() - p.h then
            p.y = love.graphics.getHeight() - p.h
        elseif p.y < 0 then
            p.y = 0
        end
    end
end

player.draw = function()
    for _,p in ipairs(player.index) do
        love.graphics.rectangle('fill', p.x, p.y, p.w, p.h)
    end
end

player.getX = function()
    for _,p in ipairs(player.index) do
        return p.x
    end
end

player.getY = function()
    for _,p in ipairs(player.index) do
        return p.y
    end
end

player.getW = function()
    for _,p in ipairs(player.index) do
        return p.w
    end
end

player.getH = function()
    for _,p in ipairs(player.index) do
        return p.h
    end
end

player.debug = function(o_x, o_y)
    for _,p in ipairs(player.index) do
        love.graphics.print("Player X : " .. p.x, x, 0)
        love.graphics.print("Player Y : " .. p.y, x, 15)

        love.graphics.print("Other Player X : " .. o_x, 0, love.graphics.getHeight() - 30)
        love.graphics.print("Other Player Y : " .. o_y, 0, love.graphics.getHeight() - 15)
    end
end

return player