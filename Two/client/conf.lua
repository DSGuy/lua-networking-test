-- Code written by Oladeji Sanyaolu (conf) 06/7/2019

love.conf = function(t)
    t.identity = "client"
    t.version  = "11.0"
    t.console  = true
  
    t.window.title          = "Client"
    t.window.icon           = nil
    t.window.width          = 400
    t.window.height         = 400
    t.window.borderless     = false
    t.window.resizable      = false
    t.window.fullscreen     = false
    t.window.fullscreentype = "exclusive"
    t.window.vsync          = true
    t.window.fsaa           = 0
    t.window.display        = 1
    t.window.highdpi        = true
    t.window.srgb           = false
  
    t.modules.physics  = false
    t.modules.mouse    = true
    t.modules.audio    = true
    t.modules.event    = true
    t.modules.graphics = true
    t.modules.image    = true
    t.modules.joystick = false
    t.modules.keyboard = true
    t.modules.math     = true
    t.modules.sound    = true
    t.modules.system   = true
    t.modules.timer    = false
    t.modules.window   = true
    t.modules.thread   = true
    t.modules.touch    = false
end
